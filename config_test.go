package config_test

import (
	"fmt"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/erdian718/config"
)

const jsonConfigData = `
{
	"Value": "value"
}
`

const xmlConfigData = `
<Config>
	<Value>value</Value>
</Config>
`

// Config represents the configuration information.
type Config struct {
	Default string
	Value   string
}

func TestMain(m *testing.M) {
	jsonConfigPath := filepath.Join(config.ExecDir(), "config.json")
	defer os.Remove(jsonConfigPath)
	if err := os.WriteFile(jsonConfigPath, append([]byte{0xEF, 0xBB, 0xBF}, jsonConfigData...), 0666); err != nil {
		panic(err)
	}

	xmlConfigPath := filepath.Join(config.ExecDir(), "config.xml")
	defer os.Remove(xmlConfigPath)
	if err := os.WriteFile(xmlConfigPath, append([]byte{0xEF, 0xBB, 0xBF}, xmlConfigData...), 0666); err != nil {
		panic(err)
	}

	m.Run()
}

func Example() {
	cfg := Config{
		Default: "default", // The default value.
	}

	if err := config.Load(&cfg, config.ExecDir(), "config.json"); err != nil {
		panic(err)
	}

	fmt.Println(cfg.Default)
	fmt.Println(cfg.Value)
	// Output:
	// default
	// value
}

func TestExecDir(t *testing.T) {
	dir1 := config.ExecDir()
	dir2 := config.ExecDir()
	if dir1 != dir2 {
		t.Fail()
	}
}

func TestHomeDir(t *testing.T) {
	dir1 := config.HomeDir()
	dir2 := config.HomeDir()
	if dir1 != dir2 {
		t.Fail()
	}
}

func TestLoadJSON(t *testing.T) {
	cfg := Config{Default: "default"}
	if err := config.Load(&cfg, config.ExecDir(), "config.json"); err != nil {
		t.Fatal(err)
	}
	if cfg.Default != "default" {
		t.Fail()
	}
	if cfg.Value != "value" {
		t.Fail()
	}
}

func TestLoadXML(t *testing.T) {
	cfg := Config{Default: "default"}
	if err := config.Load(&cfg, config.ExecDir(), "config.xml"); err != nil {
		t.Fatal(err)
	}
	if cfg.Default != "default" {
		t.Fail()
	}
	if cfg.Value != "value" {
		t.Fail()
	}
}
