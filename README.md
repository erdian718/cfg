# config

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/erdian718/config.svg)](https://pkg.go.dev/gitlab.com/erdian718/config)
[![Go Version](https://img.shields.io/badge/go%20version-%3E=1.20-61CFDD.svg?style=flat-square)](https://go.dev/)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/erdian718/config)](https://goreportcard.com/report/gitlab.com/erdian718/config)

Package config provides a simple facility for loading configuration file.

## Feature

* Very simple to use.
* Easily obtain the execution directory and home directory.

## Usage

```go
import "gitlab.com/erdian718/config"

// Config represents the configuration information.
type Config struct {
	// ...
}

func main() {
	cfg := Config{
		// The default values...
	}

	if err := config.Load(&cfg, "path", "to", "configuration", "file.json"); err != nil {
		panic(err)
	}

	// ...
}
```

## Note

* Only support JSON and XML files.
* Only support UTF-8 and UTF-8 with BOM.
