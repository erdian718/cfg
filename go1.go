//go:build go1.21

package config

import "sync"

func onceValue[T any](f func() T) func() T {
	return sync.OnceValue(f)
}
