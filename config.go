// Package config provides a simple facility for loading configuration file.
package config

import (
	"bufio"
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"os"
	"os/user"
	"path/filepath"
	"strings"
)

var (
	utf8bom = []byte{0xEF, 0xBB, 0xBF}

	getExecDir = onceValue(func() string {
		p, err := os.Executable()
		if err != nil {
			panic(fmt.Errorf("failed to get exec directory: %v", err))
		}
		return filepath.Dir(p)
	})

	getHomeDir = onceValue(func() string {
		u, err := user.Current()
		if err != nil {
			panic(fmt.Errorf("failed to get home directory: %v", err))
		}
		return u.HomeDir
	})
)

// ExecDir returns the directory of the executable that started the current process.
// This function is goroutine safe.
func ExecDir() string {
	return getExecDir()
}

// HomeDir returns the current user's home directory.
// This function is goroutine safe.
func HomeDir() string {
	return getHomeDir()
}

// Load loads configuration from file.
func Load(cfg any, elems ...string) error {
	f, err := os.Open(filepath.Join(elems...))
	if err != nil {
		return fmt.Errorf("failed to open configuration file: %v", err)
	}
	defer f.Close()

	r := bufio.NewReader(f)
	if bs, err := r.Peek(len(utf8bom)); err == nil {
		if bytes.Equal(bs, utf8bom) {
			if _, err := r.Discard(len(utf8bom)); err != nil {
				return fmt.Errorf("failed to read configuration file: %v", err)
			}
		}
	}

	ext := strings.ToLower(filepath.Ext(f.Name()))
	switch ext {
	case ".json":
		err = json.NewDecoder(r).Decode(cfg)
	case ".xml":
		err = xml.NewDecoder(r).Decode(cfg)
	default:
		err = fmt.Errorf("unsupported file type: %v", ext)
	}
	if err != nil {
		return fmt.Errorf("failed to decode configuration file: %v", err)
	}
	return nil
}
